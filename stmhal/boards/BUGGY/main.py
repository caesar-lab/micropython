# main.py -- put your code here!
import pyb
from pyb import UART

main_light = pyb.LED(1)
break_light = pyb.LED(2)
left_light = pyb.LED(3)
right_light = pyb.LED(4)

# Right Forward
timer8 = pyb.Timer(8, freq=5000)
right_forward = timer8.channel(2, pyb.Timer.PWM_INVERTED, pin=pyb.Pin.cpu.B0, pulse_width=210000)
right_forward.pulse_width_percent(0)

# Right Reverse
timer9 = pyb.Timer(9, freq=5000)
right_reverse = timer9.channel(1, pyb.Timer.PWM, pin=pyb.Pin.cpu.E5, pulse_width=210000)
right_reverse.pulse_width_percent(0)

# Left Forward
timer10 = pyb.Timer(10, freq=5000)
left_forward = timer10.channel(1, pyb.Timer.PWM, pin=pyb.Pin.cpu.B8, pulse_width=210000)
left_forward.pulse_width_percent(0)

# Left Reverse
timer11 = pyb.Timer(11, freq=5000)
left_reverse = timer11.channel(1, pyb.Timer.PWM, pin=pyb.Pin.cpu.B9, pulse_width=210000)
left_reverse.pulse_width_percent(0)

# Buzzer
timer1 = pyb.Timer(1, freq=5000)
buz = timer1.channel(1, pyb.Timer.PWM, pin=pyb.Pin.cpu.E9, pulse_width=1000)
buz.pulse_width_percent(0)

for i in range(20):
    right_light.toggle()
    left_light.toggle()
    pyb.delay(50)
	
uart = UART(3, 115200)
pyb.delay(100)
uart.init(115200, bits=8, parity=None, stop=1)
pyb.delay(100)
uart.write('$$$')
pyb.delay(500)
rep = uart.readline()
uart.write('C,0007800B0041\r')
rep = uart.readline()
pyb.delay(5000)
rep = uart.readline()

if rep == b'CONNECT failed\r\n':
    while(True):
        right_light.toggle()
        left_light.toggle()
        pyb.delay(100)

main_light.off()
uart.write('---\r')
pyb.delay(500)
rep = uart.readline()

main_light.on()
for i in range(20):
    break_light.toggle()
    pyb.delay(50)

buz.pulse_width_percent(80)
pyb.delay(100)
buz.pulse_width_percent(0)
pyb.delay(100)
buz.pulse_width_percent(50)
pyb.delay(50)
buz.pulse_width_percent(0)

while(True):
    cmd = uart.readline()
    if cmd == b'F\n':
        right_forward.pulse_width_percent(70)
        left_forward.pulse_width_percent(70)
        pyb.delay(300)
        right_forward.pulse_width_percent(0)
        left_forward.pulse_width_percent(0)
    if cmd == b'B\n':
        right_reverse.pulse_width_percent(70)
        left_reverse.pulse_width_percent(70)
        pyb.delay(300)
        right_reverse.pulse_width_percent(0)
        left_reverse.pulse_width_percent(0)
    if cmd == b'<\n':
        right_forward.pulse_width_percent(55)
        left_forward.pulse_width_percent(100)
        pyb.delay(550)
        right_forward.pulse_width_percent(0)
        left_forward.pulse_width_percent(0)
    if cmd == b'>\n':
        right_forward.pulse_width_percent(100)
        left_forward.pulse_width_percent(55)
        pyb.delay(550)
        right_forward.pulse_width_percent(0)
        left_forward.pulse_width_percent(0)
    if cmd == b'R\n':
        right_light.on()
        right_forward.pulse_width_percent(80)
        left_reverse.pulse_width_percent(80)
        pyb.delay(200)
        right_forward.pulse_width_percent(0)
        left_reverse.pulse_width_percent(0)
        while cmd == b'R\n':
            cmd = uart.readline()
        right_light.off()
    if cmd == b'L\n':
        left_light.on()
        right_reverse.pulse_width_percent(80)
        left_forward.pulse_width_percent(80)
        pyb.delay(200)
        right_reverse.pulse_width_percent(0)
        left_forward.pulse_width_percent(0)
        while cmd == b'L\n':
            cmd = uart.readline()
        left_light.off()
    if cmd == b'E\n':
        right_light.on()
        right_forward.pulse_width_percent(100)
        left_reverse.pulse_width_percent(100)
        pyb.delay(500)
        right_forward.pulse_width_percent(0)
        left_reverse.pulse_width_percent(0)
        right_light.off()
    if cmd == b'Q\n':
        left_light.on()
        right_reverse.pulse_width_percent(100)
        left_forward.pulse_width_percent(100)
        pyb.delay(500)
        right_reverse.pulse_width_percent(0)
        left_forward.pulse_width_percent(0)
        left_light.off()
    if cmd == b'S\n':
        buz.pulse_width_percent(80)
        pyb.delay(100)
        buz.pulse_width_percent(0)
        pyb.delay(100)
        buz.pulse_width_percent(50)
        pyb.delay(50)
        buz.pulse_width_percent(0)
